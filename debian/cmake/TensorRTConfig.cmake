cmake_policy(PUSH)
cmake_policy(SET CMP0057 NEW)

include(CMakeFindDependencyMacro)
set(known_components "nvinfer" "nvinfer_plugin" "nvparsers")

foreach(component nvinfer ${TensorRT_FIND_COMPONENTS})
	if(TARGET TensorRT::${component})
		continue()
	endif()

	if(NOT "${component}" IN_LIST known_components)
		message(AUTHOR_WARNING "Requested unknown TensorRT component ${component}.\nKnown components: ${known_components}")
		if(TensorRT_FIND_REQUIRED_${component})
			set(TensorRT_FOUND FALSE)
		endif()
		continue()
	endif()

	include("${CMAKE_CURRENT_LIST_DIR}/${component}.cmake" OPTIONAL RESULT_VARIABLE found)

	if(NOT found AND TensorRT_FIND_REQUIRED_${component})
		set(TensorRT_FOUND ${found})
	endif()
endforeach()

cmake_policy(POP)
