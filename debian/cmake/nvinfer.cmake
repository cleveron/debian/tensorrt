add_library(TensorRT::nvinfer SHARED IMPORTED)
set_target_properties(
	TensorRT::nvinfer PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "/usr/include"
	IMPORTED_LOCATION "/usr/lib/@DEB_HOST_MULTIARCH@/libnvinfer.so"
)
