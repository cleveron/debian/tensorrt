add_library(TensorRT::nvparsers SHARED IMPORTED)
set_target_properties(
	TensorRT::nvparsers PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "/usr/include"
	IMPORTED_LOCATION "/usr/lib/@DEB_HOST_MULTIARCH@/libnvparsers.so"
)
